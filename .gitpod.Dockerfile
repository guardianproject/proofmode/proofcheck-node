FROM gitpod/workspace-full

RUN rm -rf ~/.aws
RUN mkdir ~/.aws
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "/home/gitpod/awscliv2.zip"
RUN unzip ~/awscliv2.zip -d ~/awscliv2
RUN sudo ~/awscliv2/aws/install --update
RUN brew install aws-vault
RUN npm install -g npm-check-updates
