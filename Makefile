.PHONY: build

build: node_modules/
	npm run build

publish: build
	npm publish

node_modules/: .npmrc
	@(test -d node_modules && test -z "${CI_JOB_TOKEN}") || npm install

.npmrc:
ifdef CI_JOB_TOKEN
	echo '@guardianproject:registry=https://gitlab.com/api/v4/packages/npm/' > .npmrc
	echo '//gitlab.com/api/v4/packages/npm/:_authToken=${CI_JOB_TOKEN}' >> .npmrc
	echo '//gitlab.com/api/v4/projects/:_authToken=${CI_JOB_TOKEN}' >> .npmrc
	echo '//gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}' >> .npmrc
endif

clean:
	rm -rf build tmp

distclean: clean
	rm -rf node_modules
