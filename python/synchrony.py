from proofcheck import proofJSON
import os
import datetime
from datetime import datetime
# import timestring
import pandas as pd
import matplotlib.pyplot as plt
import json
from geojson import Feature, FeatureCollection, Point
import geopandas
import pyproj
import numpy as np
import altair as alt
import geopy
import geopy.distance
import matplotlib.pyplot as plt
from shapely.geometry import Point, Polygon
import folium
# import shapefile as shp
from shapely.geometry import Polygon
import base64
import json


def setup_path():
    env_variable = os.environ.get('PROJ_LIB')
    pyproj.datadir.set_data_dir(env_variable)


def synchrony_time_elapsed(filename):
    df = pd.read_json(filename)

    list_of_times = []

    for index, row in df.iterrows():
        file_time_string = df.loc[index, "Proof Generated"]
        list_of_times.append(str(file_time_string))

    time_initial = min(list_of_times)
    time_final = max(list_of_times)

    format = '%Y-%m-%d %H:%M:%S'
    datetime_initial = datetime.strptime(time_initial, format)
    datetime_final = datetime.strptime(time_final, format)

    time_elapsed = datetime_final - datetime_initial
    return df, time_elapsed


def synchrony_difference_between_points_in_km(df):
    geo_df = df[["DeviceID", "Location.Provider",
                 "Location.Latitude", "Location.Longitude"]]
    # geo_df["Location.Longitude"] = pd.to_numeric(geo_df["Location.Longitude"])
    numeric_longitude = pd.to_numeric(geo_df["Location.Longitude"])
    numeric_latitude = pd.to_numeric(geo_df["Location.Latitude"])
    geo_df['Location.Longitude'] = numeric_longitude
    geo_df["Location.Latitude"] = numeric_latitude
    geo_df = geo_df.dropna()
    geo_df = geo_df.drop_duplicates()

    differences_km = []
    for i, row in geo_df.iterrows():
        latitude_i = geo_df.loc[i]['Location.Latitude']
        longitude_i = geo_df.loc[i]['Location.Longitude']
        for j, row in geo_df.iterrows():
            latitude_j = geo_df.loc[j]['Location.Latitude']
            longitude_j = geo_df.loc[j]['Location.Longitude']
            if latitude_i != latitude_j and longitude_i != longitude_j:
                coords_i = (latitude_i, longitude_i)
                coords_j = (latitude_j, longitude_j)
                diff_between_one_set_of_points_km = geopy.distance.geodesic(
                    coords_i, coords_j).km
                differences_km.append(diff_between_one_set_of_points_km)
    differences_km_df = pd.DataFrame(
        {'Differences between Points in Kilometers': differences_km})
    return geo_df, differences_km_df


def syncrhony_centroid_map(geo_df):
    geojson_df = geopandas.GeoDataFrame(geo_df, geometry=geopandas.points_from_xy(
        geo_df["Location.Latitude"], geo_df['Location.Longitude']))
    x_list = geojson_df['Location.Latitude']
    y_list = geojson_df['Location.Longitude']
    center_of_coordinates = geojson_df.dissolve().centroid
    x_centroid = center_of_coordinates.centroid.x
    y_centroid = center_of_coordinates.centroid.y
    centroid_map = folium.Map(location=[x_centroid, y_centroid], zoom_start=20)
    folium.Marker(
        [x_centroid, y_centroid], popup="Centroid", icon=folium.Icon(color='blue')).add_to(centroid_map)

    all_coordinates = zip(x_list, y_list)
    for i in all_coordinates:
        folium.Marker(i, popup="Proof Points", icon=folium.Icon(
            color='green')).add_to(centroid_map)

    return geojson_df, centroid_map


def synchrony_centroid_plot(geo_df):
    geojson_df = geopandas.GeoDataFrame(geo_df, geometry=geopandas.points_from_xy(
        geo_df["Location.Latitude"], geo_df['Location.Longitude']))
    x_list = geojson_df['Location.Latitude']
    y_list = geojson_df['Location.Longitude']
    center_of_coordinates = geojson_df.dissolve().centroid
    x_centroid = center_of_coordinates.centroid.x
    y_centroid = center_of_coordinates.centroid.y
    centroid_scatter_plot = plt.figure()
    plt.scatter(x_list, y_list, c="green")
    plt.scatter(x_centroid, y_centroid, c="blue")
    plt.savefig("/centroid_scatter_plot.png")
    return geojson_df, centroid_scatter_plot


def synchrony_convex_hull(geo_df):
    geojson_df = geopandas.GeoDataFrame(geo_df, geometry=geopandas.points_from_xy(
        geo_df["Location.Latitude"], geo_df['Location.Longitude']))
    x_list = geojson_df['Location.Latitude']
    y_list = geojson_df['Location.Longitude']
    polygon_geom = Polygon(zip(x_list, y_list))
    crs = {'init': 'epsg:4326'}
    polygon = geopandas.GeoDataFrame(
        index=[0], crs=crs, geometry=[polygon_geom])
    polygon = polygon.convex_hull
    polygon.plot(figsize=(20, 20), edgecolor="purple", facecolor="purple")
    polygon.to_file(filename='polygon.shp', driver="ESRI Shapefile")
    return geojson_df, polygon


def check_synchrony(filename):
    df, time_elapsed = synchrony_time_elapsed(filename)
    geo_df, differences_km_df = synchrony_difference_between_points_in_km(df)
    geojson_df, centroid_map = syncrhony_centroid_map(geo_df)
    centroid_map.save('/centroid_map.html')
    with open("/centroid_map.html", "r", encoding='utf8') as html_file:
        centroid_map_html = ""
        for readline in html_file:
            line_strip = readline.strip()
            centroid_map_html += line_strip

    geojson_df, centroid_scatter_plot = synchrony_centroid_plot(geo_df)
    with open("/centroid_scatter_plot.png", "rb") as image_file:
        centroid_scatter_plot_image = base64.b64encode(image_file.read())

    return json.dumps({
        "time_elapsed": time_elapsed.total_seconds(),
        "geojson": json.loads(geojson_df.to_json()),
        "centroid_scatter_plot_image": centroid_scatter_plot_image.decode("utf-8"),
        "centroid_map_html": centroid_map_html
    })


check_synchrony(proofJSON)
