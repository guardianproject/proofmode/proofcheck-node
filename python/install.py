import micropip


async def load_libraries():
    await micropip.install('altair')
    await micropip.install('geojson')
    await micropip.install('geopy')
    await micropip.install('folium')
    await micropip.install('ssl')

load_libraries()
