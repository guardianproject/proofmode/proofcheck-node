from proofcheck import proofJSON
import collections
import numpy as np
import re
import pandas as pd
import json
from datetime import date
import altair as alt
alt.renderers.enable('mimetype')


def renaming_function_file(filename):
    df = pd.read_json(filename)
    new_column_names = []
    for (columnName, columnData) in df.iteritems():
        columnName_split = columnName.replace(".", " ")
        for ind, char in enumerate(columnName[:-1]):
            if columnName[ind].islower() and columnName[ind+1].isupper():
                columnName_split = columnName[:ind +
                                              1] + ' ' + columnName[ind+1:]
        new_column_names.append(columnName_split)

    df.columns = new_column_names
    return df


def binning_function(df):

    df.replace("", np.nan, inplace=True)
    # Create list of titles
    # hardware_software_metdata_list = ['Locale', 'Location Provider','IPv6', 'IPv4','Language', 'Network Type', 'Network', 'Manufacturer', 'Data Type', 'Hardware', 'Screen Size', 'Cell Info']
    metadata_list = df.columns

    # Initiate device ID dataframe
    DeviceID_List = []

    # Create new dataframe for pivot table, with Unique Device IDs as the column headers, and hardware/software features as the row index
    for i in list(df['Device ID']):
        if i not in DeviceID_List:
            DeviceID_List.append(i)

    device_df = pd.DataFrame(columns=DeviceID_List, index=metadata_list)

    # Group number of each unique feature by device ID.
    # (For example, if there are two language per device ID, count "2")
    # For most features, there will be 1 feature per device ID.

    for i, row in device_df.iterrows():
        aggregate_df = df.groupby("Device ID")[i].nunique()
        device_df = device_df.append(aggregate_df)
        # device_df = pd.concat(device_df, aggregate_df)

    # device_df = device_df.fillna(0)

    # This is a REALLY SLOPPY way of doing this, but it's what worked quickest so I'm leaving it for now.
    # If you're translating this into JavaScript, a better option would be to replace each line based on the index

    device_df = device_df.dropna()

    # In a future version of the code, I'm going to change the initiation of the DeviceID Dataframe to avoid having to transpose it later on.
    # Trial and error process of working with the histogram and the pivot table, sorry about that!
    transpose_df = device_df.T

    return device_df, transpose_df


def outliers_histogram_robust(df, transpose_df):
    outliers_df = pd.DataFrame()
    list_of_device_IDs = []
    list_of_device_ID_occurences = []
    list_of_attributes_which_have_1_to_1_correspondence = [
        'File Hash SHA256', 'File Modified', 'File Path', 'Proof Generated']
    list_of_non_1_to_1_occurences = []
    normal_key_list = []
    normal_value_list = []
    outlier_key_list = []
    outlier_value_list = []
    for label, content in transpose_df.iteritems():
        for number_attributes in content.values:
            if number_attributes != 1:
                outliers_df[label] = content
        if label == 'Device ID':
            for j in content.index:
                list_of_device_IDs.append(j)
                list_of_device_ID_occurences.append(
                    df['Device ID'].value_counts()[j])
        device_occurence_dict = dict(
            zip(list_of_device_IDs, list_of_device_ID_occurences))
        device_occurence_dict = collections.OrderedDict(
            sorted(device_occurence_dict.items()))
    for k in list_of_attributes_which_have_1_to_1_correspondence:
        i = 0
        for key, value in device_occurence_dict.items():
            grouped_1_to_1_attributes = transpose_df[k].groupby(level=0).sum()
            if grouped_1_to_1_attributes.index[i] == key and grouped_1_to_1_attributes[i] == value:
                normal_key_list.append(key)
                normal_value_list.append(value)
            else:
                outlier_key_list.append(key)
                outlier_value_list.append(value)
            i += 1

    normal_dict = dict(zip(normal_key_list, normal_value_list))
    outlier_dict = dict(zip(outlier_key_list, outlier_value_list))

    for i in outliers_df.index:
        for attribute in list_of_attributes_which_have_1_to_1_correspondence:
            for key, value in normal_dict.items():
                if i == key:
                    outliers_df.loc[i, attribute] = np.nan

    outliers_df = outliers_df.replace(1, np.nan)
    outliers_df = outliers_df.dropna(how='all')
    outliers_list = list(outliers_df.columns.values)

    return outliers_df, outliers_list, outlier_dict

# Extract headers from column names


def interactive_histogram(outliers_df, outliers_list):

    source = outliers_df
    selector = alt.selection_single(fields=['Device Attribute'], bind='legend')

    base = alt.Chart(source).properties(
        width=250,
        height=250
    ).transform_fold(
        outliers_list,
        as_=['Device Attribute', 'Unique Devices']
    ).add_selection(
        selector
    )

    chart = base.mark_bar().encode(
        x=alt.X('Unique Devices:Q', axis=alt.Axis(
            title='Number of Attributes per Device', tickMinStep=1)),
        y=alt.Y('count()', stack=None, axis=alt.Axis(
            title='Number of Unique Devices', tickMinStep=1)),
        color=alt.Color('Device Attribute:N',
                        scale=alt.Scale(scheme='category20b')),
        opacity=alt.condition(selector, alt.value(1), alt.value(0.03))
    ).properties(
        title='Histogram of Attributes Across Devices'
    )

    chart.configure_title(
        fontSize=20,
        font='Tahoma',
        anchor='start',
        color='grey')

    return chart


def summary_of_flags(outliers_df):
    df_summary_of_flags = pd.DataFrame()
    df_summary_of_flags['Number of Flags'] = outliers_df.count(axis='columns')
    json_summary_of_flags = df_summary_of_flags.to_json(orient="split")
    return json_summary_of_flags


def check_consistency(filename):
    df = renaming_function_file(filename)
    device_df, transpose_df = binning_function(df)
    outliers_df, outliers_list, outlier_dict = outliers_histogram_robust(
        df, transpose_df)
    chart = interactive_histogram(outliers_df, outliers_list)
    # chart.show()
    df_summary_of_flags = summary_of_flags(outliers_df)
    return json.dumps({
        "df_summary_of_flags": df_summary_of_flags,
        "outliers_list": outliers_list,
        "chart": chart.to_json()
    })


check_consistency(proofJSON)
