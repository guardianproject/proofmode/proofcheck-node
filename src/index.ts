import { fetchCID, addText } from "./ipfs.js";
import { checkIntegrity } from "./integrity.js";
import { checkConsistency } from "./consistency.js";
import { checkSynchrony } from "./synchrony.js";
import { unzipFile, findMediaFiles, hashFile, getRelatedFiles, setupPyodide, createCombinedJSON } from "./utils.js";

/*
const createSummary = (fileData: Record<string, any>) => {
  console.log(fileData);
  const summary = {

  }

  return summary;
}
*/

const checkCommon = async (zipFilePath: string) => {
  const proofDir = await unzipFile(zipFilePath);
  const mediaFiles = await findMediaFiles(proofDir);

  let final = {
    summary: {}
  };

  for (const fileName of mediaFiles) {
    const fileHash = await hashFile(proofDir, fileName);
    const relatedFiles = await getRelatedFiles(proofDir, fileName);
    const integrity = await checkIntegrity(proofDir, relatedFiles);

    final[fileHash] = {
      files: relatedFiles,
      integrity,
    };
  }

  const proofJSON = await createCombinedJSON(proofDir, mediaFiles);
  const proofContext = { proofJSON }
  const pyodide = await setupPyodide(proofContext);
  final['consistency'] = await checkConsistency(pyodide, proofContext);
  final['synchrony'] = await checkSynchrony(pyodide, proofContext);
  final.summary['cid'] = await addText(JSON.stringify(final));

  return final;
}

export const checkCID = async (cid: string) => {
  const zipFilePath = await fetchCID(cid);
  return await checkCommon(zipFilePath);
};

export const checkZip = async (zipFilePath: string) => {
  return await checkCommon(zipFilePath);
};
