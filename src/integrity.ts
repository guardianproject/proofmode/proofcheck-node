import * as jose from "jose";
import openpgp from "openpgp";
import OpenTimestamps from "opentimestamps";
import { promises as fs } from "fs";
import { RelatedFiles } from "./utils.js";
/* import { createC2pa } from 'c2pa';

const checkC2PA = async (): Promise<any> => {
  const sampleImage =
    'https://raw.githubusercontent.com/contentauth/c2pa-js/main/tools/testing/fixtures/images/CAICAI.jpg';

  const c2pa = await createC2pa({
    wasmSrc: '/wasm/toolkit_bg.wasm',
    workerSrc: '/wasm/c2pa.worker.min.js',
  });

  try {
    const { manifestStore } = await c2pa.read(sampleImage);
    console.log('manifestStore', manifestStore);

    // Get the active manifest
    const activeManifest = manifestStore?.activeManifest;
    console.log('activeManifest', activeManifest);

    return { activeManifest }
  } catch (err) {
    console.error('Error reading image:', err);
    return { activeManifest: null }
  }
}
*/
const checkOpenTimestamp = async (proofDir: string, fileName: string) => {
  try {
    const rawFile = (await fs.readFile(`${proofDir}/${fileName}`, "utf8")).toString();
    console.log({ rawFile })
    const fileOts = Buffer.from((await fs.readFile(`${proofDir}/${fileName}`, "utf8")).toString());
    console.log({ fileOts })

    const originalFileOts = Buffer.from(
      "004f70656e54696d657374616d7073000050726f6f6600bf89e2e884e89294010832bb24ab386bef01c0656944ecafa2dbb1e4162ced385754419467f9fb6f4d97f010c7c118043ce37d45f1ab81d3cd9dc9aa08fff0109b01031328e457c754a860bc5bc567ab08f02012dbcf25d46d7f01c4bd7c7ebdcd2080974b83a9198bc63cdb23f69c817f110508f0203c6274f7a67007de279fb68938e5549f462043570ccdbc17ba43e632a772d43208f1045ab0daf9f008ad9722b721af69e80083dfe30d2ef90c8e292868747470733a2f2f66696e6e65792e63616c656e6461722e657465726e69747977616c6c2e636f6df010dfd289ba718b4f30bb78191936c762a508f02026503e60c641473ec6f833953d04f7c8a65c5059a44a7e8c01c8cb9fed2ac2b308f1045ab0dafaf008c0c7948d8d5b64cf0083dfe30d2ef90c8e232268747470733a2f2f6c74632e63616c656e6461722e636174616c6c6178792e636f6d",
      "hex"
    );

    const detached = OpenTimestamps.DetachedTimestampFile.deserialize(fileOts);
    console.log({ detached })
    const infoResult = OpenTimestamps.info(detached);
    console.log({ infoResult })
    return infoResult
    // console.log({ originalFileOts })
    /*
    const detached = OpenTimestamps.DetachedTimestampFile.fromBytes(
      new OpenTimestamps.Ops.OpSHA256(),
      file
    );
    const detachedOts = OpenTimestamps.DetachedTimestampFile.deserialize(fileOts);
    const verifyResult = await OpenTimestamps.verify(detachedOts, detached, {
      timeout: 5000,
      ignoreBitcoinNode: true,
    });

    return verifyResult;
    */
  } catch (e) {
    return { error: e.toString() }
  }
};

const checkPGP = async (proofDir: string, publicKeyFileName: string, fileName: string, signatureFileName: string, binary: boolean = false) => {
  const publicKeyString = await fs.readFile(`${proofDir}/${publicKeyFileName}`, "ascii");
  const publicKey = await openpgp.readKey({ armoredKey: publicKeyString });
  let message: openpgp.Message<string>;

  if (binary) {
    const fileData = await fs.readFile(`${proofDir}/${fileName}`);
    message = await openpgp.createMessage({ binary: fileData });
    console.log(message);
  } else {
    const messageString = await fs.readFile(`${proofDir}/${fileName}`, "utf8");
    message = await openpgp.createMessage({ text: messageString });
  }
  const signatureString = await fs.readFile(`${proofDir}/${signatureFileName}`, "ascii");
  console.log({ signatureString })
  const signature = await openpgp.readSignature({
    armoredSignature: signatureString,
  })
  console.log({ signature })
  const verificationResult = await openpgp.verify({
    message,
    signature,
    verificationKeys: publicKey,
  });
  console.log({ verificationResult })
  const { keyID, verified: v, signature: s } = verificationResult.signatures[0];
  const out = { key: keyID.toHex(), verified: false, createdAt: null };

  try {
    const verified = await v; // throws on invalid signature
    const sig = await s;
    out.verified = verified;
    out.createdAt = sig.packets[0].created;
  } catch (e) {
    console.log({ e })
  } finally {
    return out;
  }
};

const checkSafetyNet = async (proofDir: string, fileName?: string) => {
  if (!fileName) {
    return {}
  }

  const jwt = await fs.readFile(`${proofDir}/${fileName}`, "ascii");
  /*
  const certString = `-----BEGIN CERTIFICATE-----
    MIIFbzCCBFegAwIBAgIRAP4UU7By5wpREFFKwIqks44wDQYJKoZIhvcNAQELBQAw
    RjELMAkGA1UEBhMCVVMxIjAgBgNVBAoTGUdvb2dsZSBUcnVzdCBTZXJ2aWNlcyBM
    TEMxEzARBgNVBAMTCkdUUyBDQSAxRDQwHhcNMjIwNTE5MTIwMzI0WhcNMjIwODE3
    MTIwMzIzWjAdMRswGQYDVQQDExJhdHRlc3QuYW5kcm9pZC5jb20wggEiMA0GCSqG
    SIb3DQEBAQUAA4IBDwAwggEKAoIBAQDgn3Xmw1zq9y3yrAZAxTs1ztTFoXaK7DbJ
    sejbWZx+wzkuYa9r4hg25JAGVq7z16CNtsW9XvNpc5HcLovn/oS/kiPsW+hOKhrP
    GPGBxK/OQHkKyHUcej50FtNBLOG5V7Vsw8VjxF5COVMCebvwwYJqx6ghGoKdk2cr
    ePS2hb+/Jp7SfPeZpZfyPYAbHVzS4W2ZLIj9CgsKOkvKhuXeVyv1+py9l3SWjsmi
    qQOvmEs+n+pqxAj+e1DbOUFjzX5Zf3rIYFubua861GwKSwcyVO4WfTd9QwzXYH9H
    VMkVWJaQs+nKEIN8//7vnLwBoDsV8c56GfwSgVxyDg5mwowRAwATAgMBAAGjggJ/
    MIICezAOBgNVHQ8BAf8EBAMCBaAwEwYDVR0lBAwwCgYIKwYBBQUHAwEwDAYDVR0T
    AQH/BAIwADAdBgNVHQ4EFgQU4arkLvXyL3etEbDTwBjGX8qHMGUwHwYDVR0jBBgw
    FoAUJeIYDrJXkZQq5dRdhpCD3lOzuJIwewYIKwYBBQUHAQEEbzBtMDgGCCsGAQUF
    BzABhixodHRwOi8vb2NzcC5wa2kuZ29vZy9zL2d0czFkNGludC9vUldMQTJWOGxk
    YzAxBggrBgEFBQcwAoYlaHR0cDovL3BraS5nb29nL3JlcG8vY2VydHMvZ3RzMWQ0
    LmRlcjAdBgNVHREEFjAUghJhdHRlc3QuYW5kcm9pZC5jb20wIQYDVR0gBBowGDAI
    BgZngQwBAgEwDAYKKwYBBAHWeQIFAzA/BgNVHR8EODA2MDSgMqAwhi5odHRwOi8v
    Y3Jscy5wa2kuZ29vZy9ndHMxZDRpbnQvX0ZQcXFJSGdYNjguY3JsMIIBBAYKKwYB
    BAHWeQIEAgSB9QSB8gDwAHYARqVV63X6kSAwtaKJafTzfREsQXS+/Um4havy/HD+
    bUcAAAGA3Gp6vAAABAMARzBFAiBSj7BNIfmjbvndlwJ5yYE8OkX8SkzelQk5f4RD
    WdqxpwIhAPcQi72oNSOHl3lik27iNS+5rmWkY5w5WQFjOJuf5m2sAHYAQcjKsd8i
    RkoQxqE6CUKHXk4xixsD6+tLx2jwkGKWBvYAAAGA3Gp60gAABAMARzBFAiAlnC8C
    6ZzjgvTBWzNv5AsrAraE8U1lCb/m2Rgg5JgcDAIhAOB3kTEmnAucyr1xajEn57EP
    gcPg0DywXgkXM3d1UdXpMA0GCSqGSIb3DQEBCwUAA4IBAQB41MX1DI+a9AWPNcir
    C34LasF2hxldXXI5UD1fcnIlarnzmLsTurIF7m0j3ULvwU2+g5jy+xy6QCORuMFv
    KNPd2NVGUvOytj/nYr7Oa9tyAyRR79ZgOooEgRWVrWYzG2/JVXB3itVzBCZyClPA
    KXnSXQQsjOJ3HhiLjUHfaYxPYtCOwKGufxhVw/ptzlLB4HQgqIYvT8mJ84mZ8dhA
    9BJ6qQHPVuI3CuSR5TLdCICozDAaTsQg4g6H1X3WwA6scmbL/lAv4gInKS8TKwpJ
    y9XN3wv3ixtYeZpH3HvVYbtbbfLKJ5YONeoIvbCChGz9fk4GmPLf3kI7Sq3g/V7I
    rwO1
    -----END CERTIFICATE-----`;
  const cert = new X509Certificate(certString);

  const { payload, protectedHeader } = await jose.jwtVerify(jwt, cert.raw, {
    issuer: "urn:example:issuer",
    audience: "urn:example:audience",
  });
  console.log({ payload, protectedHeader });
  */

  const json = jose.decodeJwt(jwt) //, null, "RS256");

  return json;
};

export const checkIntegrity = async (proofDir: string, relatedFiles: RelatedFiles) => {
  const {
    publicKey,
    mediaFile,
    mediaFileSignature,
    proofCSV,
    proofCSVSignature,
    proofJSON,
    proofJSONSignature,
    gst
  } = relatedFiles;
  const pgp = {
    mediaFile: await checkPGP(proofDir, publicKey, mediaFile, mediaFileSignature, true),
    proofCSV: await checkPGP(proofDir, publicKey, proofCSV, proofCSVSignature),
    proofJSON: await checkPGP(proofDir, publicKey, proofJSON, proofJSONSignature)
  }
  const safetyNet = await checkSafetyNet(proofDir, gst);
  const { ots: otsFile } = relatedFiles;
  const ots = await checkOpenTimestamp(proofDir, otsFile);
  // const c2pa = await checkC2PA();

  return { pgp, safetyNet, ots, c2pa: null }
};
