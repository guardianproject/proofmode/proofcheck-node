import crypto from "crypto";
import { promises as fs } from "fs";
import decompress from "decompress";
import { loadPyodide } from "pyodide";
import fetch from "node-fetch";
import { fileURLToPath } from 'url';
import { dirname } from "path";
import ImageData from "@canvas/image-data";
import { JSDOM } from "jsdom";

export type RelatedFiles = {
  publicKey: string | null
  mediaFile: string | null
  mediaFileSignature: string | null
  proofCSV: string | null
  proofCSVSignature: string | null
  proofJSON: string | null
  proofJSONSignature: string | null
  ots: string | null
  gst: string | null
}

export const getRelatedFiles = async (directory: string, fileName: string): Promise<RelatedFiles> => {
  const fileHash = await hashFile(directory, fileName);
  const fileNameOrNull = async (directory: string, fileName: string): Promise<string | null> => {
    const path = `${directory}/${fileName}`
    const exists = !!(await fs.stat(path).catch(_ => false));
    return exists ? fileName : null;
  }

  return {
    publicKey: await fileNameOrNull(directory, 'pubkey.asc'),
    mediaFile: fileName,
    mediaFileSignature: await fileNameOrNull(directory, `${fileHash}.asc`),
    proofCSV: await fileNameOrNull(directory, `${fileHash}.proof.csv`),
    proofCSVSignature: await fileNameOrNull(directory, `${fileHash}.proof.csv.asc`),
    proofJSON: await fileNameOrNull(directory, `${fileHash}.proof.json`),
    proofJSONSignature: await fileNameOrNull(directory, `${fileHash}.proof.json.asc`),
    ots: await fileNameOrNull(directory, `${fileHash}.ots`),
    gst: await fileNameOrNull(directory, `${fileHash}.gst`)
  }
};

export const findFirstFile = async (directory: string, endFileName: string): Promise<string> => {
  const files = await fs.readdir(directory);
  const fileName = files.find((file) => file.endsWith(endFileName));

  return `${directory}/${fileName}`;
};

export const hashFile = async (directory: string, fileName: string): Promise<string> => {
  const fileBuffer = await fs.readFile(`${directory}/${fileName}`);
  const hashSum = crypto.createHash('sha256');
  hashSum.update(fileBuffer);
  const hash = hashSum.digest('hex');

  return hash;
}

export const unzipFile = async (zipPath: string): Promise<string> => {
  const directoryName = crypto.randomUUID();
  const extractedFilePath = `/tmp/${directoryName}`;
  await decompress(zipPath, extractedFilePath);

  return extractedFilePath;
};

export const findMediaFiles = async (directory: string): Promise<string[]> => {
  const files = await fs.readdir(directory);
  const mediaFileTypes = ["png", "jpg", ".jpeg", "aac", "mp3", "mp4", "mov", "wav", "ogg", "aif", "aiff"];
  const mediaFiles = files.filter(file => mediaFileTypes.includes(file.split(".").pop().toLowerCase()));

  return mediaFiles;
};

export const createCombinedJSON = async (directory: string, fileNames: string[]): Promise<string> => {
  const combinedJSON = await Promise.all(fileNames.map(async (fileName: string) => {
    const { proofJSON } = await getRelatedFiles(directory, fileName);
    const proofJSONString = await fs.readFile(`${directory}/${proofJSON}`, 'utf8');
    return JSON.parse(proofJSONString);
  }));

  return JSON.stringify(combinedJSON);
}

export const setupPyodide = async (proofcheckContext: any): Promise<any> => {
  const currentDirectory = dirname(fileURLToPath(import.meta.url));
  const install = await fs.readFile(`${currentDirectory}/python/install.py`, "utf8");
  const { document } = (new JSDOM(`...`)).window;
  const pyodide = await loadPyodide({
    indexURL: `./node_modules/pyodide`,
    jsglobals: { fetch, Object, ImageData, document, clearInterval, setInterval, clearTimeout, setTimeout, },
  });
  await pyodide.loadPackage(["micropip", "numpy", "matplotlib", "pandas", "geopandas", "pyproj", "shapely"])
  await pyodide.runPythonAsync(install);
  pyodide.registerJsModule("proofcheck", proofcheckContext);

  return pyodide;
}
