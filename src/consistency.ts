import { promises as fs } from "fs";
import { fileURLToPath } from 'url';
import { dirname } from "path";

export const checkConsistency = async (pyodide: any, proofcheckContext: any) => {
  const currentDirectory = dirname(fileURLToPath(import.meta.url));
  const consistency = await fs.readFile(`${currentDirectory}/python/consistency.py`, "utf8");
  const response = await pyodide.runPythonAsync(consistency, proofcheckContext);
  const parsedSpec = JSON.parse(response);

  return parsedSpec;
};
