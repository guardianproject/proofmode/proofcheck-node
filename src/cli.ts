#!/usr/bin/env node

import yargs from 'yargs';
import { hideBin } from 'yargs/helpers'
import { checkCID, checkZip } from './index.js';

yargs(hideBin(process.argv))
    .command('check-cid <cid>', 'fetch the CID from IPFS, check its proof', () => { }, async (argv) => {
        const { cid } = argv;
        const result = await checkCID(cid as string);
        console.log(JSON.stringify(result, null, 2));
        process.exit(0);
    })
    .demandCommand(1)
    .argv;

yargs(hideBin(process.argv))
    .command('check-zip <zipFilePath>', 'extract zip at path, check its proof', () => { }, async (argv) => {
        const { zipFilePath } = argv;
        const result = await checkZip(zipFilePath as string);
        console.log(JSON.stringify(result, null, 2));
        process.exit(0);
    })
    .demandCommand(1)
    .argv;

