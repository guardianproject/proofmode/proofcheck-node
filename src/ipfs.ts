import * as IPFS from "ipfs-http-client";
import { promises as fs } from "fs";

export const fetchCID = async (cid: string): Promise<string> => {
  const ipfs = await IPFS.create(new URL(process.env.IPFS_API_URL));

  try {
    const fileData = [];
    for await (const chunk of ipfs.cat(cid)) {
      // @ts-ignore
      fileData.push(chunk);
    }

    const fileName = cid.split("/").pop();
    const filePath = `/tmp/${fileName}`;
    await fs.writeFile(filePath, fileData);

    return filePath;
  } catch (error) {
    console.log(error);
    return "";
  }
};

export const addText = async (text: string): Promise<string> => {
  const ipfs = await IPFS.create(new URL(process.env.IPFS_API_URL));

  try {
    const { cid } = await ipfs.add(text);

    return cid.toString();
  } catch (error) {
    console.log(error);
    return "";
  }
};

