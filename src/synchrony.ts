import { promises as fs } from "fs";
import { fileURLToPath } from 'url';
import { dirname } from "path";

export const checkSynchrony = async (pyodide: any, proofcheckContext: any) => {
  const currentDirectory = dirname(fileURLToPath(import.meta.url));
  const synchrony = await fs.readFile(`${currentDirectory}/python/synchrony.py`, "utf8");
  const response = await pyodide.runPythonAsync(synchrony, proofcheckContext);
  const parsedResponse = JSON.parse(response);

  return parsedResponse;
};
